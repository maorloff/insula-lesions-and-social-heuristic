%load('data/conformityBehavior')
load('data/controlReplication')

rightSide = [.122 .443 .231];
leftSide = [.757 .251 .157];

manualXLims = [-3 3];
figure; hold on;
plot([manualXLims(1) manualXLims(2)],[0 0],'k--','LineWidth',1.5)
ylim([-2.25 0.75])
xlim(manualXLims)
plot([0 0],[-2.25 0.75],'k--','LineWidth',1.5)

scatter(logit_OmegaOCU,rLOFC,70,'MarkerFaceColor',rightSide,'MarkerEdgeColor','k','LineWidth',1.2)
xMinMax1 = [min(logit_OmegaOCU),max(logit_OmegaOCU)];
groupSolveAt1 = linspace(xMinMax1(1), xMinMax1(2));
p1 = polyfit(logit_OmegaOCU,rLOFC,1);
p1plot = polyval(p1,groupSolveAt1);
set(gca,'XLim',xMinMax1)
a = plot(groupSolveAt1,p1plot);
set(a,'color',leftSide,'LineWidth',2)

scatter(logit_OmegaOCU,lLOFC,70,'MarkerFaceColor',leftSide,'MarkerEdgeColor','k','LineWidth',1.2)
xMinMax2 = [min(logit_OmegaOCU),max(logit_OmegaOCU)];
groupSolveAt2 = linspace(xMinMax1(1), xMinMax1(2));
p2 = polyfit(logit_OmegaOCU,lLOFC,1);
p2plot = polyval(p2,groupSolveAt1);
set(gca,'XLim',xMinMax2)
b = plot(groupSolveAt2,p2plot);
set(b,'color',rightSide,'LineWidth',2)

ylim([-2.25 0.75])
set(gca,'YTick',[-2.25:.75:.75]);
xlim(manualXLims)

[r,p] = corr(logit_OmegaOCU,rLOFC);
disp(['correlation between logit(follow weight) and rLPFC beta, r = ' num2str(r) ', P = ' num2str(p)])
[r,p] = corr(logit_OmegaOCU,lLOFC);
disp(['correlation between logit(follow weight) and lLPFC beta, r = ' num2str(r) ', P = ' num2str(p)])

logit_OmegaOCU_insula = log((1-medianPosteriorOmegaOCU_insula) ./ (1 - (1-medianPosteriorOmegaOCU_insula)));

[y1, x1] = ksdensity(logit_OmegaOCU);
[y2, x2] = ksdensity(logit_OmegaOCU_insula);

currYlim = ylim;
scaleHeight = 2.5;
a1 = area(x1,y1.*scaleHeight+currYlim(1),currYlim(1),'FaceColor',colorCC,'LineWidth',2);
a2 = area(x2,y2.*scaleHeight+currYlim(1),currYlim(1),'FaceColor',colorINSULA,'LineWidth',2);
a1.FaceAlpha = 0.7; a2.FaceAlpha = 0.7;
axis square
