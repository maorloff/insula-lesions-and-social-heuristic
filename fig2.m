figure; set(gcf,'Units','inches','Position', [5 5 20 4]);

if ~exist('data/hybridrerun.mat')
    load('data/OCU_socialReliance_hybrid') 
else
    load('data/hybridrerun')
    medianPosteriorOmegaFollow_control = medianPosteriorOmegaFollow(1:28);
    medianPosteriorOmegaOppose_control = medianPosteriorOmegaOppose(1:28);
    medianPosteriorOmegaOCU_control = medianPosteriorOmegaOCU(1:28);
    medianPosteriorOmegaFollow_insula = medianPosteriorOmegaFollow(29:38);
    medianPosteriorOmegaOppose_insula = medianPosteriorOmegaOppose(29:38);
    medianPosteriorOmegaOCU_insula = medianPosteriorOmegaOCU(29:38);
    medianPosteriorOmegaFollow_acc = medianPosteriorOmegaFollow(39:44);
    medianPosteriorOmegaOppose_acc = medianPosteriorOmegaOppose(39:44);
    medianPosteriorOmegaOCU_acc = medianPosteriorOmegaOCU(39:44);
end

%%2a-c
for i = 1:3
    switch i
        case 1
            currParam_control = medianPosteriorOmegaFollow_control;
            currParam_insula = medianPosteriorOmegaFollow_insula;
            currParam_acc = medianPosteriorOmegaFollow_acc;
            paramLabel = 'OCU-free follow weight';
        case 2
            currParam_control = medianPosteriorOmegaOppose_control;
            currParam_insula = medianPosteriorOmegaOppose_insula;
            currParam_acc = medianPosteriorOmegaOppose_acc;
            paramLabel = 'OCU-free oppose weight';
        case 3
            currParam_control = medianPosteriorOmegaOppose_control;
            currParam_insula = medianPosteriorOmegaOppose_insula;
            currParam_acc = medianPosteriorOmegaOppose_acc;
            paramLabel = 'OCU weight';
    end
    
    subplot(1,4,i); hold on;
    bar(1,mean(currParam_control),'FaceColor',colorCONTROL,'EdgeColor','k','LineWidth',1.5)
    bar(2,mean(currParam_insula),'FaceColor',colorINSULA,'EdgeColor','k','LineWidth',1.5)
    bar(3,mean(currParam_acc),'FaceColor',colorACC,'EdgeColor','k','LineWidth',1.5)
    errorbar([1; 2; 3], [mean(currParam_control),mean(currParam_insula),mean(currParam_acc)], ...
        [SEM(currParam_control),SEM(currParam_insula),SEM(currParam_acc)], ...
        'k.','LineWidth',1.5)
    
    scatter(reshape(repmat([1],28,1),1,[]), reshape(currParam_control,1,[]), 25, [.5 .5 .5],'filled', 'jitter','on','jitterAmount',0.1,'LineWidth',1.5);
    scatter(reshape(repmat([2],10,1),1,[]), reshape(currParam_insula,1,[]), 25, [.5 .5 .5],'filled', 'jitter','on','jitterAmount',0.1,'LineWidth',1.5);
    scatter(reshape(repmat([3],6,1),1,[]), reshape(currParam_acc,1,[]), 25, [.5 .5 .5],'filled', 'jitter','on','jitterAmount',0.1,'LineWidth',1.5);
    
    ylim([0 1])
    
    disp(' ')
    disp([paramLabel ' between groups:'])
    disp(['Control v lesion: P = ' num2str(bootstrapTtest(currParam_control,[currParam_insula; currParam_acc]))])
end

%%2d
subplot(1,4,4); hold on;
scatter(LL_control,medianPosteriorOmegaFollow_control, 40,'filled','MarkerFaceColor',colorCONTROL,'MarkerEdgeColor',[0 0 0],'LineWidth',1);
scatter(LL_insula,medianPosteriorOmegaFollow_insula, 40,'filled','MarkerFaceColor',colorINSULA,'MarkerEdgeColor',[0 0 0],'LineWidth',1);
scatter(LL_acc,medianPosteriorOmegaFollow_acc, 40,'filled','MarkerFaceColor',colorACC,'MarkerEdgeColor',[0 0 0],'LineWidth',1);

LLs = [LL_control;LL_insula;LL_acc];

mdl = fitlm(LLs, medianPosteriorOmegaFollow, 'linear');
xDataLims = [min(LLs) max(LLs)];
predLine = predict(mdl,xDataLims');
plot(xDataLims,predLine,'color',[0 0 0],'LineWidth',1.5)

ylim([0 1])

[r,p] = corr(LLs,medianPosteriorOmegaFollow);
disp(' ')
disp(['correlation between negative log likelihoods and OCU-free follow weight, r = ' num2str(round(r,4,'significant')) ', P = ' num2str(round(p,4,'significant'))])
disp(' ')
