figure; set(gcf,'Units','inches','Position', [5 5 3 4]); hold on;

%%1d
if ~exist('data/soloRPrerun.mat')
    load('data/soloRP_SXXX') %soloRP, solo only
    LL_control = median(log_lik_control,2).*-1;
    LL_insula = median(log_lik_insula,2).*-1;
    LL_acc = median(log_lik_acc,2).*-1;
else
    load('data/soloRPrerun')
    LL_control = median(log_lik(1:28,:),2).*-1;
    LL_insula = median(log_lik(29:38,:),2).*-1;
    LL_acc = median(log_lik(39:44,:),2).*-1;
end

bar(1,mean(LL_control),'FaceColor',colorCONTROL,'EdgeColor','k','LineWidth',1.5)
bar(2,mean(LL_insula),'FaceColor',colorINSULA,'EdgeColor','k','LineWidth',1.5)
bar(3,mean(LL_acc),'FaceColor',colorACC,'EdgeColor','k','LineWidth',1.5)
errorbar([1; 2; 3], [mean(LL_control),mean(LL_insula),mean(LL_acc)], ...
    [SEM(LL_control),SEM(LL_insula),SEM(LL_acc)], ...
    'k.','LineWidth',1.5)

scatter(reshape(repmat([1],28,1),1,[]), reshape(LL_control,1,[]), 25, [.5 .5 .5],'filled', 'jitter','on','jitterAmount',0.1,'LineWidth',1.5);
scatter(reshape(repmat([2],10,1),1,[]), reshape(LL_insula,1,[]), 25, [.5 .5 .5],'filled', 'jitter','on','jitterAmount',0.1,'LineWidth',1.5);
scatter(reshape(repmat([3],6,1),1,[]), reshape(LL_acc,1,[]), 25, [.5 .5 .5],'filled', 'jitter','on','jitterAmount',0.1,'LineWidth',1.5);

disp('negative log likelihood between groups:')
disp(['Control v lesion: P = ' num2str(bootstrapTtest(LL_control,[LL_insula; LL_acc]))])
disp(['Control v insula: P = ' num2str(bootstrapTtest(LL_control,LL_insula))])
disp(['Control v acc: P = ' num2str(bootstrapTtest(LL_control,LL_acc))])
disp(['Acc v insula: P = ' num2str(bootstrapTtest(LL_acc,LL_insula))])
