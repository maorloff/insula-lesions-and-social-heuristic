function P = bootstrapRtoZ(r1, N1, sampleR1, r2, N2, sampleR2)
%nonparametric bootstrap (i.e., sample with replacement)
boot_iters = 10000;
[~, trueZ] = rtoz(r1,N1,r2,N2); %real test

boot_stat_vals = NaN(1,boot_iters);%row1 cont v ins, row2 cont v acc, row3 acc v insula

for i = 1:boot_iters
    [~,z] = rtoz(sampleR1(i),N1,sampleR2(i),N2);
    boot_stat_vals(1,i) = z;
end

P = sum(abs(boot_stat_vals(1,:))>=abs(trueZ))/boot_iters;
end

