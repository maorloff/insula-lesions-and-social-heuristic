function P = bootstrapBartlett(group1,group2,group3)

[~, stats] = vartestn([group1;group2;group3],[ones(1,length(group1))';2*ones(1,length(group2))';3*ones(1,length(group3))'],'Display','off');
trueChiSquare = stats.chisqstat;

P_val = [];
boot_iters = 10000;
%%bootstrap solo stat-vals%%
boot_stat_vals = NaN(1,boot_iters);%row1 cont v ins, row2 cont v acc, row3 acc v insula

all_samples = [group1;group2;group3];
N1 = length(group1);
N2 = length(group2);
N3 = length(group3);

for i = 1:boot_iters
    sample1 = datasample(all_samples,N1,1);
    sample2 = datasample(all_samples,N2,1);
    sample3 = datasample(all_samples,N3,1);
    [p, stats] = vartestn([sample1;sample2;sample3],[ones(1,N1)';2*ones(1,N2)';3*ones(1,N3)'],'Display','off');
    boot_stat_vals(1,i) = stats.chisqstat;
end

P = sum(abs(boot_stat_vals)>=abs(trueChiSquare))/boot_iters;

end

