data {
  int<lower=1> NumObservation;
  int<lower=1> NumSubject;
  int<lower=0,upper=1> behavCol_group[NumObservation]; //choice
  real probCol_group[NumObservation];                //probability
  real payoffCol_group[NumObservation,4];           //payoff
  int<lower=0,upper=1> insulaIndicator[NumSubject]; //indicator for dummy - insula
  int<lower=0,upper=1> accIndicator[NumSubject]; //indicator for dummy - acc
}

parameters { #anything with a prior goes here
  vector[4] mu_p; 		      //mean of parameters
  vector<lower=0>[2] s_p; 	//standard deviation of parameters
  vector[NumSubject] beta_raw;
  vector[NumSubject] rho_raw;
}

transformed parameters { #transformations on parameters (as name suggests)
  vector<lower=0>[NumSubject] beta;      //inverse temp parameter
  vector<lower=0>[NumSubject] rho;       //riskPref parameter 
  
  for (sub in 1:NumSubject) {
  beta[sub] = 50*Phi_approx(mu_p[1] + insulaIndicator[sub] * mu_p[3] + accIndicator[sub] * mu_p[4] + s_p[1] * beta_raw[sub]);
  }
  rho = exp(mu_p[2] + s_p[2] * rho_raw);
}


model {
  int initIndex;
  
  # hyperparameters
  mu_p ~ normal(0,10);		
  s_p  ~ cauchy(0,2.5);   
  
  beta_raw ~ normal(0,1.0);
  rho_raw  ~ normal(0,1.0);
  
  initIndex = 1;
  for (subjIndex in 1:NumSubject) {
  for (trialIndex in initIndex:(initIndex+(24)-1)) { //24 trials
  real Util_safe;
  real Util_risky;

  Util_safe  = probCol_group[trialIndex] * pow(payoffCol_group[trialIndex,1], rho[subjIndex]);
  Util_safe  = Util_safe + (1-probCol_group[trialIndex]) * pow(payoffCol_group[trialIndex,2], rho[subjIndex]);
  
  Util_risky = probCol_group[trialIndex] * pow(payoffCol_group[trialIndex,3], rho[subjIndex]);
  Util_risky = Util_risky + (1-probCol_group[trialIndex]) * pow(payoffCol_group[trialIndex,4], rho[subjIndex]);
  
  behavCol_group[trialIndex]~ bernoulli_logit(beta[subjIndex] * (Util_safe - Util_risky)); #functions as softmax
  }
  
  initIndex = initIndex+(24);
  }
}

generated quantities {
  real log_lik[NumSubject];
  int initIndex;

  initIndex = 1;
    for (subjIndex in 1:NumSubject) {
      log_lik[subjIndex] = 0;

      for (trialIndex in initIndex:(initIndex+(24)-1)) {
        real Util_safe;
        real Util_risky;

        Util_safe  = probCol_group[trialIndex] * pow(payoffCol_group[trialIndex,1], rho[subjIndex]);
        Util_safe  = Util_safe + (1-probCol_group[trialIndex]) * pow(payoffCol_group[trialIndex,2], rho[subjIndex]);

        Util_risky = probCol_group[trialIndex] * pow(payoffCol_group[trialIndex,3], rho[subjIndex]);
        Util_risky = Util_risky + (1-probCol_group[trialIndex]) * pow(payoffCol_group[trialIndex,4], rho[subjIndex]);

        log_lik[subjIndex] = log_lik[subjIndex] + bernoulli_logit_lpmf(behavCol_group[trialIndex] | beta[subjIndex] * (Util_safe - Util_risky));

      }
    initIndex = initIndex+(24);
  }

}
