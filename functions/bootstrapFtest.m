function P = bootstrapFtest(group1,group2)
%nonparametric bootstrap (i.e., sample with replacement)
boot_iters = 10000;

[~, ~, ~, stats] = vartest2(group1,group2);
trueF = stats.fstat;

boot_stat_vals = NaN(1,boot_iters);%row1 cont v ins, row2 cont v acc, row3 acc v insula

alldata = [group1;group2];
N1 = length(group1);
N2 = length(group2);

for i = 1:boot_iters
    sample1 = datasample(alldata,N1);
    sample2 = datasample(alldata,N2);
    [~, ~, ~, stats] = vartest2(sample1,sample2);
    boot_stat_vals(1,i) = stats.fstat;
end

P = sum(abs(boot_stat_vals(1,:))>=abs(trueF))/boot_iters;
end

