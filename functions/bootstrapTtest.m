function P = bootstrapTtest(group1,group2)
%nonparametric bootstrap (i.e., sample with replacement)
boot_iters = 10000;
[h,p,ci,stats] = ttest2(group1,group2); %'real' test
trueT = stats.tstat;
boot_stat_vals = NaN(1,boot_iters);
alldata = [group1;group2];
group1N = length(group1); group2N = length(group2);

for i = 1:boot_iters
    group1sample = datasample(alldata,group1N);
    group2sample = datasample(alldata,group2N);
    [h,p,ci,stats] = ttest2(group1sample,group2sample);
    boot_stat_vals(1,i) = stats.tstat;
end

P = sum(abs(boot_stat_vals(1,:))>=abs(trueT))/boot_iters;
end
