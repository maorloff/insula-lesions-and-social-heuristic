function distances = diagonalDistance_rotate_plot(x,y)
cAll=[x y];

tempListAll=[cAll];

Tmat=[cos(-pi/4) sin(-pi/4) 0; -sin(-pi/4) cos(-pi/4) 0; 0 0 1];
T=maketform('affine',Tmat);
[allT]=tformfwd(T,tempListAll);

distances = allT(:,2);
end