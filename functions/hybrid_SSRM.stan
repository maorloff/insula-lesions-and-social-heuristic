data {
  int<lower=1> NumObservation;
  int<lower=1> NumSubject;
  int<lower=0,upper=1> behavCol_group[NumObservation]; //choice
  real probCol_group[NumObservation];                //probability
  real payoffCol_group[NumObservation,4];           //payoff
  int<lower=0,upper=1> conditionCol_group[NumObservation,2];  //condition 
  int<lower=0,upper=1> insulaIndicator[NumSubject]; //indicator for dummy - insula
  int<lower=0,upper=1> accIndicator[NumSubject]; //indicator for dummy - acc
}

parameters {
  vector[5] mu_p; 		      //mean of parameters
  vector<lower=0>[5] s_p; 	//standard deviation of parameters
  
  //1st level - subject level//
  vector[NumSubject] beta_raw;
  vector[NumSubject] rho_raw;
  vector[NumSubject] ocu_raw;
  vector[NumSubject] delta_beta_insula_raw;
  vector[NumSubject] delta_beta_acc_raw;
  simplex[3] omega[NumSubject];
}

transformed parameters { #transformations on parameters (as name suggests)
  vector<lower=0>[NumSubject] beta;      //inverse temp param (1 per subject)
  vector<lower=0>[NumSubject] rho;       //riskPref parameter 
  vector[NumSubject] ocu;
  vector[NumSubject] delta_beta_insula;
  vector[NumSubject] delta_beta_acc;

  beta = exp(mu_p[1] + s_p[1] * beta_raw);
  rho  = exp(mu_p[2] + s_p[2] * rho_raw);
  ocu = mu_p[3] + s_p[3] * ocu_raw;
  delta_beta_insula = mu_p[4] + s_p[4] * delta_beta_insula_raw;
  delta_beta_acc = mu_p[5] + s_p[5] * delta_beta_acc_raw;
}

model {
  int initIndex;
  
  # hyperparameters
  mu_p ~ normal(0,10);						//top level priors- v.wide
	
  s_p  ~ cauchy(0,2.5);

  # individual parameters w/ Matt trick FOR TRANSFORMATIONS
  beta_raw ~ normal(0,1.0); //ocu temp
  rho_raw  ~ normal(0,1.0);
  ocu_raw ~ normal(0,1.0);
  delta_beta_insula_raw ~ normal(0,1.0);
  delta_beta_acc_raw ~ normal(0,1.0);

  initIndex = 1;
  for (subjIndex in 1:NumSubject) {
  for (trialIndex in initIndex:(initIndex+95)) { //96 trials
  real Util_safe;
  real Util_risky;
  int info_trial;
  int safe_trial;
  int risky_trial;
  real ps[3];

  //FOR OCU
  Util_safe = probCol_group[trialIndex] * pow(payoffCol_group[trialIndex,1], rho[subjIndex]);
  Util_safe = Util_safe + (1-probCol_group[trialIndex]) * pow(payoffCol_group[trialIndex,2], rho[subjIndex]);
  Util_safe = Util_safe + conditionCol_group[trialIndex,1]* ocu[subjIndex];
  
  Util_risky = probCol_group[trialIndex] * pow(payoffCol_group[trialIndex,3], rho[subjIndex]);
  Util_risky = Util_risky + (1-probCol_group[trialIndex]) * pow(payoffCol_group[trialIndex,4], rho[subjIndex]);
  Util_risky = Util_risky + conditionCol_group[trialIndex,2]* ocu[subjIndex];
  
  //FOR SOCIAL RELIANCE PART
  safe_trial = conditionCol_group[trialIndex,1];
  risky_trial = conditionCol_group[trialIndex,2];
  
  info_trial = logical_neq(safe_trial, risky_trial);
  
  //mix models here:
  if (info_trial==1) {
  ps[1] = log(omega[subjIndex,1]) + bernoulli_logit_lpmf( behavCol_group[trialIndex] | 50 * ( safe_trial - risky_trial) ); // blind following
  ps[2] = log(omega[subjIndex,2]) + bernoulli_logit_lpmf( behavCol_group[trialIndex] | -50 * (safe_trial - risky_trial) ); // blind opposing
  ps[3] = log(omega[subjIndex,3]) + bernoulli_logit_lpmf( behavCol_group[trialIndex] | (beta[subjIndex] + insulaIndicator[subjIndex] * delta_beta_insula[subjIndex] + accIndicator[subjIndex] * delta_beta_acc[subjIndex] ) * (Util_safe-Util_risky) );
  target +=  log_sum_exp(ps);
  } else if (info_trial == 0) {
  target += bernoulli_logit_lpmf( behavCol_group[trialIndex] | (beta[subjIndex] + insulaIndicator[subjIndex] * delta_beta_insula[subjIndex] + accIndicator[subjIndex] * delta_beta_acc[subjIndex] ) * (Util_safe-Util_risky) );
  }
  } 
  initIndex = initIndex+96;
  }
}

generated quantities {
real log_lik[NumSubject];
int initIndex;

initIndex = 1;
for (subjIndex in 1:NumSubject) {
log_lik[subjIndex] = 0;

for (trialIndex in initIndex:(initIndex+95)) {
real Util_safe;
real Util_risky;
int info_trial;
int safe_trial;
int risky_trial;
real ps[3];

//FOR SOLO RP
  Util_safe = probCol_group[trialIndex] * pow(payoffCol_group[trialIndex,1], rho[subjIndex]);
  Util_safe = Util_safe + (1-probCol_group[trialIndex]) * pow(payoffCol_group[trialIndex,2], rho[subjIndex]);
  Util_safe = Util_safe + conditionCol_group[trialIndex,1]* ocu[subjIndex];
  
  Util_risky = probCol_group[trialIndex] * pow(payoffCol_group[trialIndex,3], rho[subjIndex]);
  Util_risky = Util_risky + (1-probCol_group[trialIndex]) * pow(payoffCol_group[trialIndex,4], rho[subjIndex]);
  Util_risky = Util_risky + conditionCol_group[trialIndex,2]* ocu[subjIndex];
  
//FOR SOCIAL RELIANCE PART
safe_trial = conditionCol_group[trialIndex,1];
risky_trial = conditionCol_group[trialIndex,2];

info_trial = logical_neq(safe_trial, risky_trial);

if (info_trial==1) {
ps[1] = log(omega[subjIndex,1]) + bernoulli_logit_lpmf( behavCol_group[trialIndex] | 50 * ( safe_trial - risky_trial) ); // blind following
ps[2] = log(omega[subjIndex,2]) + bernoulli_logit_lpmf( behavCol_group[trialIndex] | -50 * (safe_trial - risky_trial) ); // blind opposing
ps[3] = log(omega[subjIndex,3]) + bernoulli_logit_lpmf( behavCol_group[trialIndex] | (beta[subjIndex] + insulaIndicator[subjIndex] * delta_beta_insula[subjIndex] + accIndicator[subjIndex] * delta_beta_acc[subjIndex] ) * (Util_safe-Util_risky) );
log_lik[subjIndex] = log_lik[subjIndex] + log_sum_exp(ps);
} else if (info_trial == 0) {
log_lik[subjIndex] = log_lik[subjIndex] + bernoulli_logit_lpmf( behavCol_group[trialIndex] | (beta[subjIndex] + insulaIndicator[subjIndex] * delta_beta_insula[subjIndex] + accIndicator[subjIndex] * delta_beta_acc[subjIndex] ) * (Util_safe-Util_risky) );
}
} 
initIndex = initIndex+96;
}
}
