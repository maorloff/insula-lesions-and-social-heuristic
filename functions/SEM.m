function StandardError = SEM(data)

while sum(isnan(data)) > 0
    to_remove = isnan(data);
    data(to_remove) = [];
end

StandardError = std(data)/sqrt(length(data));

end