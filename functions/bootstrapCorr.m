function [P, r, sampleDist] = bootstrapCorr(x,y)
%nonparametric bootstrap (i.e., sample with replacement)
boot_iters = 10000;

[trueR] = corr(x,y); %'real' test

boot_stat_vals = NaN(1,boot_iters);
N = length(x);

for i = 1:boot_iters
    xSample = datasample(x,N);
    ySample = datasample(y,N);
    
    r = corr(xSample,ySample);
    boot_stat_vals(1,i) = r;
end

P = sum(abs(boot_stat_vals(1,:))>=abs(trueR))/boot_iters;
sampleDist = boot_stat_vals;
r = trueR;
end
