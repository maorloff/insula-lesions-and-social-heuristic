# insula lesions and social reliance

These are the scripts necessary to reproduce the figures in Orloff, Chung, & Gu et al., 2022
All data necessary to reproduce figures is included in this repository.
To make these figures run 'insulaLesionSocialReliance.m' from its containing folder.
If you would like to re-estimate model parameters, run 'estimate_soloRP.R' and/or 'estimate_hybrid.R'
When you run 'insulaLesionSocialReliance.m' again, it will automatically include the re-estimated parameters (assuming you didn't change any file paths...).
